##Avira 

Contributors: specia
Requires at least: WordPress 4.4
Tested up to: WordPress 4.9.8
Stable tag: 1.0.49
Version: 1.0.49
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: custom-logo, translation-ready, blog

Avira WordPress Theme, Copyright 2018 Specia Theme
Avira is distributed under the terms of the GNU GPL

##Description

Avira is the right theme for you if you are looking for a multipurpose theme that can handle everything you throw at it. It is perfect for marketing, corporate startup, agency, blog, business, company, creative, portfolio, professional business, food & restaurant, gym & fitness, spa salon, medical practitioner & hospitals, landing pages, product pages, corporate business, digital agency, product showcase, financial advisor, accountant, law firm, wealth advisor, photography, personal, and any eCommerce stores. The design is easy to change and adapt to the client needs.

=== Frequently Asked Questions ===

1. Get Support - https://specia.ticksy.com
2. In your admin panel, go to Appearance > Themes and click the Add New button.
3. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
4. Click Activate to use your new theme right away.

## Credit & Copyright

Avira WordPress Theme is child theme of Specia WordPress Theme, Copyright 2018 Specia Theme
Avira WordPress Theme is distributed under the terms of the GNU GPL

i) Package Structure
Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

ii) Font Awesome
Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
Source: http://fontawesome.io

iii) Bootstrap Framework
Bootstrap (http://getbootstrap.com/)
Copyright (c) 2011-2014 Twitter, Inc
Licensed - https://github.com/twbs/bootstrap/blob/master/LICENSE

iv) Owl Carousel 2
Owl Carousel 2 - by @David Deutsch - https://github.com/OwlCarousel2
Licensed - https://github.com/OwlCarousel2/OwlCarousel2/blob/develop/LICENSE

v) Simple Text Rotator
Simple Text Rotator - https://github.com/peachananr/simple-text-rotator
Licensed - https://github.com/peachananr/simple-text-rotator/blob/master/LICENSE

vi) Animate Css
Animate Css by @Daniel Eden - https://daneden.me/
License - https://github.com/daneden/animate.css/blob/master/LICENSE (MIT License)
Source: https://github.com/daneden/animate.css

vii) Sticky Js
Sticky Js by @Anthony Garand - http://stickyjs.com/
License - https://github.com/garand/sticky/blob/master/LICENSE.md
Source: http://stickyjs.com/

viii) Modernizr Custom
Modernizr Custom 
License - MIT & BSD
Source: http://modernizr.com/download/#-touch-shiv-cssclasses-teststyles-prefixes-load

ix) WOW Js
WOW Js by @matthieu
License - GNU GPL license v3
Source: https://github.com/matthieua/WOW


Images
=================================================================================================
Screenshot Image
URL: https://www.pexels.com/photo/man-holding-teacup-infront-of-laptop-on-top-of-table-inside-the-room-925786/
Source: https://www.pexels.com/
License: CC0 License

##Changelog
version 1.0.49
* Added Theme Tag (grid-layout)

version 1.0.48
* Added Theme Tag

version 1.0.47
* Improve Screenshot

version 1.0.46
* Changed Premium Themes Links

version 1.0.45
* Added Documentation, Support & Review Buttons

version 1.0.44
* Added Parent Theme License

version 1.0.43
* Added Premium Themes Link

version 1.0.42
* Theme Details URL changed

version 1.0.41
* Added support link in readme

version 1.0.40
* WordPress 4.9.7 Compatibility Tested.

version 1.0.39
* Social Media Improvement Test against Parent theme

version 1.0.38
* WordPress 4.9.7 Compatibility Tested.

version 1.0.37
* Update code for security purpose

version 1.0.36
* Service icon effects changed

version 1.0.35
* Code beautify

version 1.0.34
* Screenshot changed

version 1.0.33
* Added [wp_reset_postdata();] for slider.

version 1.0.32
* Added [wp_reset_postdata();] for slider.

version 1.0.31
* Updated License Information

version 1.0.30
* WordPress 4.9.6 Compatibility Tested.

version 1.0.29
* Changed Screenshot and Copyright

version 1.0.28
* Added [wp_reset_postdata();] for templates.

version 1.0.27
* Output escaping issue resolved.

version 1.0.25
* Resolved color issue in editor-style.css

version 1.0.24
* WordPress 4.9.5 Compatibility Tested.

version 1.0.23
* Resolved Slider Bug

version 1.0.22
* Resolved Slider Bug

version 1.0.21
* Screenshot Changed

version 1.0.20
* Screenshot Changed

version 1.0.19
* WordPress 4.9.4 Compatibility Tested.

version 1.0.18
* Code Improvement.

version 1.0.17
* WordPress 4.9.2 Compatibility Tested.

version 1.0.16
* Image URL changed.

version 1.0.15
* Screenshot Changed.

version 1.0.14
* WordPress 4.9.1 compatibility tested.
* Customizer Improvment(Allow Addition True).

version 1.0.13
* WordPress 4.9 compatibility tested.

version 1.0.12
* Tested Latest WordPress version compatibility

version 1.0.11
* Responsive Bug Resolved

version 1.0.10
* Screenshot Changed

version 1.0.8
* Enhance Theme Detail

version 1.0.7
* Description Improvement

version 1.0.6
* Sanitization Issue Fixed

version 1.0.5
* Replace Screenshot Image

version 1.0.4
* Version Error Fixed

version 1.0.3
* Social Media Features Added

version 1.0.2
* Theme Review Issues Fixed

version 1.0.1
* Initial release

version 1.0
* Initial release
